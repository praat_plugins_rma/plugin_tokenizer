# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).


## [Unrealised]

### Added
  - Added commands

## 1.0.0

### Added
  - Added commands (Fixed menu)
    - `Tokenize TextGrids from folder...`
    - `Settings...`
    - `About`
  - Added commands (Dynamic menu)
    - `Add tokenized tier...`